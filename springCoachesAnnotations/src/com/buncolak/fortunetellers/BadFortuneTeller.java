package com.buncolak.fortunetellers;

import org.springframework.stereotype.Component;

@Component
public class BadFortuneTeller implements FortuneTeller {
    @Override
    public String giveFortune() {
        return "You may die today.";
    }
}
