package com.buncolak.fortunetellers;

public interface FortuneTeller {
    String giveFortune();
}
