package com.buncolak.coaches;

import com.buncolak.fortunetellers.FortuneTeller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("anotherCoach")
public class BasketballCoach implements Coach {
    private FortuneTeller fortuneTeller;

    @PostConstruct
    public void getBalls() {
        System.out.println("Coach is getting balls");
    }

    @Override
    public String getDailyWorkout() {
        return "Beat GSW.";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }

    public FortuneTeller getFortuneTeller() {
        return fortuneTeller;
    }

    @Autowired
    @Qualifier("happyFortuneTeller")
    public void setFortuneTeller(FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }
}
