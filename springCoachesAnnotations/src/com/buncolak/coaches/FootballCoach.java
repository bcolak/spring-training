package com.buncolak.coaches;

import com.buncolak.fortunetellers.FortuneTeller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("myCoach")
public class FootballCoach implements Coach {

    private FortuneTeller fortuneTeller;

    @Autowired
    public FootballCoach(@Qualifier("badFortuneTeller") FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }

    @Override
    public String getDailyWorkout() {
        return "Study penalties";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }
}
