package com.buncolak.coaches;

import com.buncolak.fortunetellers.FortuneTeller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class TennisCoach implements Coach {
    @Autowired
    @Qualifier("happyFortuneTeller")
    private FortuneTeller fortuneTeller;

    @PostConstruct
    public void getRackets() {
        System.out.println("Coach is getting rackets");
    }

    @PreDestroy
    public void takeThrashOut(){
        System.out.println("Coach is taking the thrash out.");
    }

    @Override
    public String getDailyWorkout() {
        return "Try not to lose.";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }
}
