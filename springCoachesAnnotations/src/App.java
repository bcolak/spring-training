import com.buncolak.coaches.Coach;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        Coach coach = applicationContext.getBean("myCoach", Coach.class);
        System.out.println(coach.getDailyWorkout());
        System.out.println(coach.getDailyFortune());

        System.out.println();

        Coach anotherCoach = applicationContext.getBean("anotherCoach", Coach.class);
        System.out.println(anotherCoach.getDailyWorkout());
        System.out.println(anotherCoach.getDailyFortune());

        System.out.println();

        Coach myCoach = applicationContext.getBean("tennisCoach", Coach.class);
        System.out.println(myCoach.getDailyWorkout());
        System.out.println(myCoach.getDailyFortune());

        applicationContext.close();
    }
}
