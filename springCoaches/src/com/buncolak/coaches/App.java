package com.buncolak.coaches;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext
                = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        Coach coach = applicationContext.getBean("myCoach", Coach.class);

        System.out.println(coach.getDailyWorkout());
        System.out.println(coach.getDailyFortune());
        System.out.println("My name is: " +  coach.getName());
        System.out.println("My email is: " + coach.getEmail());

        Coach anotherCoach = applicationContext.getBean("myCoach", Coach.class);
        System.out.println("Are two coaches same? " + (anotherCoach == coach));

        applicationContext.close();
    }
}
