package com.buncolak.coaches;

import com.buncolak.fortunetellers.BadFortuneTeller;
import com.buncolak.fortunetellers.FortuneTeller;

public class BasketballCoach implements Coach {
    private FortuneTeller fortuneTeller;
    private String email;
    private String name;

    public BasketballCoach() {
    }

    @Override
    public String getDailyWorkout() {
        return "Play like LeBron";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }

    public FortuneTeller getFortuneTeller() {
        return fortuneTeller;
    }

    public void setFortuneTeller(FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
