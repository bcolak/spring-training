package com.buncolak.coaches;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
    String getEmail();
    String getName();
}
