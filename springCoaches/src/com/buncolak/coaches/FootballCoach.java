package com.buncolak.coaches;

import com.buncolak.fortunetellers.FortuneTeller;

public class FootballCoach implements Coach {
    private FortuneTeller fortuneTeller;
    private String name;
    private String email;

    public FootballCoach() {
    }

    public void preparePitch() {
        System.out.println("Coach is preparing the pitch.");
    }

    public void turnOffTheLights() {
        System.out.println("Coach is turning the lights off.");
    }

    @Override
    public String getDailyWorkout() {
        return "Study penalties";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }

    public FortuneTeller getFortuneTeller() {
        return fortuneTeller;
    }

    public void setFortuneTeller(FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
