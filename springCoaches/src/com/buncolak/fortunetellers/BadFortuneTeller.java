package com.buncolak.fortunetellers;

public class BadFortuneTeller implements FortuneTeller {
    @Override
    public String giveFortune() {
        return "You'll live long, but you'll be coding PHP!";
    }
}
