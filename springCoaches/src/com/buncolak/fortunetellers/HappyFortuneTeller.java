package com.buncolak.fortunetellers;

public class HappyFortuneTeller implements FortuneTeller {
    @Override
    public String giveFortune() {
        return "You'll live long!";
    }
}
