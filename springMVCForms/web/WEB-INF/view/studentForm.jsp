<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: bcolak
  Date: 3.10.2018
  Time: 09:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Form</title>
</head>
<body>

<form:form modelAttribute="student" action="showStudent">
    First Name: <form:input path="firstName"/>
    Last Name: <form:input path="lastName" />
    <br>
    <form:select path="country">
        <form:options items="${student.countries}" />
    </form:select>
    <br>
    <form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguages}" />
    <br>
    <form:checkboxes path="operatingSystem" items="${student.operatingSystems}" />
    <br>
    <form:button name="submit" value="Submit">Submit</form:button>
</form:form>

</body>
</html>
