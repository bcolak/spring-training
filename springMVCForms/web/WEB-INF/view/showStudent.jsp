<jsp:useBean id="student" scope="request" type="com.buncolak.model.Student"/>
<%--
  Created by IntelliJ IDEA.
  User: bcolak
  Date: 3.10.2018
  Time: 09:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show Student</title>
</head>
<body>
Student: ${student.firstName} ${student.lastName}
<br>
Country: ${student.country}
<br>
Favorite Lang: ${student.favoriteLanguage}
<br>
OS: ${student.operatingSystem}
</body>
</html>
