package com.buncolak.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Student {
    private String firstName;
    private String lastName;
    private String country;
    private String favoriteLanguage;
    private String operatingSystem;

    private LinkedHashMap<String, String> countries;
    private ArrayList<String> favoriteLanguages;
    private ArrayList<String> operatingSystems;

    public Student() {
        countries = new LinkedHashMap<>();
        countries.put("TR", "Turkey");
        countries.put("BR", "Brazil");
        countries.put("US", "United States");
        countries.put("DE", "Germany");
        countries.put("UK", "United Kingdom");

        favoriteLanguages = new ArrayList<>();
        favoriteLanguages.add("Java");
        favoriteLanguages.add("C#");
        favoriteLanguages.add("Kotlin");
        favoriteLanguages.add("Javascript");

        operatingSystems = new ArrayList<>();
        operatingSystems.add("Linux");
        operatingSystems.add("OS X");
        operatingSystems.add("Windows");
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public ArrayList<String> getOperatingSystems() {
        return operatingSystems;
    }

    public void setOperatingSystems(ArrayList<String> operatingSystems) {
        this.operatingSystems = operatingSystems;
    }

    public String getFavoriteLanguage() {
        return favoriteLanguage;
    }

    public void setFavoriteLanguage(String favoriteLanguage) {
        this.favoriteLanguage = favoriteLanguage;
    }

    public ArrayList<String> getFavoriteLanguages() {
        return favoriteLanguages;
    }

    public void setFavoriteLanguages(ArrayList<String> favoriteLanguages) {
        this.favoriteLanguages = favoriteLanguages;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LinkedHashMap<String, String> getCountries() {
        return countries;
    }

    public void setCountries(LinkedHashMap<String, String> countries) {
        this.countries = countries;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
