package com.buncolak.controller;

import com.buncolak.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("form")
public class StudentForm {
    @GetMapping("showForm")
    public String showForm(Model model) {
        Student student = new Student();
        model.addAttribute("student",student);
        return "studentForm";
    }

    @PostMapping("showStudent")
    public String showStudent(@ModelAttribute Student student){
        return "showStudent";
    }
}
