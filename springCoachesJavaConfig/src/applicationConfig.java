import com.buncolak.coaches.BasketballCoach;
import com.buncolak.coaches.Coach;
import com.buncolak.fortuneteller.BadFortuneTeller;
import com.buncolak.fortuneteller.FortuneTeller;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.buncolak")
@PropertySource("classpath:application.properties")
public class applicationConfig {
    @Bean
    public FortuneTeller myFortuneTeller() {
        return new BadFortuneTeller();
    }

    @Bean
    public Coach anotherCoach() {
        return new BasketballCoach(myFortuneTeller());
    }
}
