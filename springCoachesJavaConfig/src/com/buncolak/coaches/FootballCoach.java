package com.buncolak.coaches;

import com.buncolak.fortuneteller.FortuneTeller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("myCoach")
public class FootballCoach implements Coach {
    @Value("${coach.name}")
    private String name;

    @Value("${coach.email}")
    private String email;


    private FortuneTeller fortuneTeller;

    @Autowired
    public FootballCoach(@Qualifier("happyFortuneTeller") FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }

    @Override
    public String getDailyWorkout() {
        return "Study Penalties";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
