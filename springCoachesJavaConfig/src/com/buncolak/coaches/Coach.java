package com.buncolak.coaches;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortune();
    String getName();
    String getEmail();
}
