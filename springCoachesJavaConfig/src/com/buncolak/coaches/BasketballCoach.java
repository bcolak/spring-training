package com.buncolak.coaches;

import com.buncolak.fortuneteller.FortuneTeller;
import org.springframework.beans.factory.annotation.Value;

public class BasketballCoach implements Coach {
    @Value("${coach.name}")
    private String name;

    @Value("${coach.email}")
    private String email;

    private FortuneTeller fortuneTeller;

    public BasketballCoach(FortuneTeller fortuneTeller) {
        this.fortuneTeller = fortuneTeller;
    }

    @Override
    public String getDailyWorkout() {
        return "Study dribbling";
    }

    @Override
    public String getDailyFortune() {
        return fortuneTeller.giveFortune();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
