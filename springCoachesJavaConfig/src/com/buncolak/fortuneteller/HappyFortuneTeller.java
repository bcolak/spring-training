package com.buncolak.fortuneteller;

import org.springframework.stereotype.Component;

@Component
public class HappyFortuneTeller implements FortuneTeller {
    @Override
    public String giveFortune() {
        return "You'll live long!";
    }
}
