package com.buncolak.fortuneteller;

public interface FortuneTeller {
    String giveFortune();
}
