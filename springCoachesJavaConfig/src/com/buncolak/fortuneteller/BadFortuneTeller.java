package com.buncolak.fortuneteller;

public class BadFortuneTeller implements FortuneTeller {
    @Override
    public String giveFortune() {
        return "You'll die today!";
    }
}
