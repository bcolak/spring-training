import com.buncolak.coaches.Coach;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(applicationConfig.class);

        Coach coach = applicationContext.getBean("myCoach", Coach.class);
        System.out.println(coach.getDailyWorkout());
        System.out.println(coach.getDailyFortune());

        System.out.println();

        Coach anotherCoach = applicationContext.getBean("anotherCoach", Coach.class);
        System.out.println(anotherCoach.getDailyWorkout());
        System.out.println(anotherCoach.getDailyFortune());
        System.out.println("Coach name: " + anotherCoach.getName());
        System.out.println("Coach email: " + anotherCoach.getEmail());

        applicationContext.close();
    }
}
