<%--
  Created by IntelliJ IDEA.
  User: bcolak
  Date: 24.09.2018
  Time: 13:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Simple Form</title>
    <link href="${pageContext.request.contextPath}/resources/css/base.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<form action="${pageContext.request.contextPath}/form/processForm" method="get">
    Please enter your name: <input type="text" name="name">
    <button class="btn-primary btn" type="submit">Submit</button>
</form>

<form action="${pageContext.request.contextPath}/form/processModelForm" method="POST">
    Please enter your name: <input type="text" name="name">
    <button class="btn btn-primary" type="submit">Submit with model</button>
</form>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
