package com.buncolak.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("form")
public class SimpleForm {

    @RequestMapping("/showForm")
    public String showForm() {
        return "showSimpleForm";
    }

    @RequestMapping("/processForm")
    public String processForm() {
        return "processSimpleForm";
    }

    // HttpServletRequest not working. Why?
    @PostMapping("/processModelForm")
    public String processModelForm(@RequestParam String name, Model model) {
        System.out.println(name);
        model.addAttribute("name", name);
        return "processModelForm";
    }
}
